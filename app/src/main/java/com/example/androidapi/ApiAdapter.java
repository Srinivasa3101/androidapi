package com.example.androidapi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ApiAdapter extends RecyclerView.Adapter<ApiAdapter.ApiViewHolder> {
    private ArrayList<ApiRepo> apiRepos;
    private Context context;

    public ApiAdapter(ArrayList<ApiRepo> apiRepos, Context context){
        this.apiRepos = apiRepos;
        this.context = context;
    }

    public static  class ApiViewHolder extends  RecyclerView.ViewHolder{
        public ImageView imageView;
        public Context context;

        public ApiViewHolder(View view){
            super(view);
            imageView = view.findViewById(R.id.imageView);
        }
    }

    @NonNull
    @Override
    public ApiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from((parent.getContext())).inflate(R.layout.apilist, parent, false);
        ApiViewHolder apiViewHolder = new ApiViewHolder(view);
        return apiViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ApiViewHolder holder, int position) {
        final ApiRepo thisItem = apiRepos.get(position);
        Glide.with(context).load(thisItem.getImg()).placeholder(R.drawable.prog)
        .diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return apiRepos.size();
    }
}
