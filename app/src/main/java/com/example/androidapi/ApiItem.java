package com.example.androidapi;

public class ApiItem {
    private String imgUrl;

    public ApiItem(String imgUrl){
        this.imgUrl = imgUrl;
    }

    public final String getImgUrl(){
        return imgUrl;
    }
}
