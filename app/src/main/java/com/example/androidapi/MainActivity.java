package com.example.androidapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ArrayList<ApiRepo> apiRepos = new ArrayList<>();
    private RecyclerView recyclerView;
    private  ApiAdapter apiAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        apiAdapter = new ApiAdapter(apiRepos, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(apiAdapter);

        String API_BASE_URL = "https://androido-api.herokuapp.com/v1/";


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()
                );

        Retrofit retrofit = builder.build();

        ApiClient client =  retrofit.create(ApiClient.class);

        Call<ArrayList<ApiRepo>> call = client.repoForUser();

        call.enqueue(new Callback<ArrayList<ApiRepo>>() {
            @Override
            public void onResponse(Call<ArrayList<ApiRepo>> call, Response<ArrayList<ApiRepo>> response) {
                Log.e("REQUEST",call.request().url().toString());

                if (response.body() != null) {
                    Log.e("RESPONSE",response.body().toString());
                }

                ArrayList<ApiRepo> repos = response.body();

                if(repos !=null){
                    apiRepos.addAll(repos);
                    apiAdapter.notifyDataSetChanged();
                }
                else{
                    Log.e("RESPONSE",response.message());
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ApiRepo>> call, Throwable t) {
               t.printStackTrace();
            }
        });

    }
}