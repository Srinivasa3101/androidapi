package com.example.androidapi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiClient {

    @GET("images")
    Call<ArrayList<ApiRepo>> repoForUser();


}
